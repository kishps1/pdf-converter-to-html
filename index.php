

  <?php
  include($_SERVER['DOCUMENT_ROOT'] . '/lib/pdfnew.php');
  include($_SERVER['DOCUMENT_ROOT'] . '/lib/config.php');

  // Возвращаем данные в формате json
  header('Content-Type: application/json');


  $request = $_REQUEST;

  //Проверяем токен
  $token = $request['token'];
  if (TOKEN && $token != TOKEN) die();

  
  $action = $request['action'];  
  $link = $request['link'];
  $fileName = $request['fileName'];

  $newPdf = new PdfNew($link);

  switch ($action) {
    //Получить файл в формате base64
    case 'getFile':
      $result = $newPdf->getPdfBase64();
      break;
    //Получить ссылку для скачивания
    case 'getSrcDownload':
      if ($fileName)
      $result = $newPdf->getSrcDownload($fileName);
      break;

    default:
      $result = ['status' => 'error', 'message' => 'Не задано действие action'];
      break;
  }




  echo json_encode($result);


  ?>

