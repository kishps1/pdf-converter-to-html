<?


class PdfNew
{
    public $link = '';

    public $filename = '';

    function __construct($link)
    {

        $this->link = $link;
    }


    /**
     * Возвращает строку base64 из файла
     * @return array ['base64string'=>$base64string, 'status'=>'success'] or [ 'status'=>'error']
     */
    public function getPdfBase64()
    {
        $filename = 'tmp';
        $link = $this->link;

        $cd =  $_SERVER['DOCUMENT_ROOT'] . '/tmp';

        $fileResultPath = $cd . '/' . $filename . ".pdf";

        $command = "wkhtmltopdf " . $link . " " . $filename . ".pdf";

        $resExec = exec("cd " . $cd . '; ' . $command . '; ');

        if (file_exists($fileResultPath)) {

            $base64string = base64_encode(file_get_contents($fileResultPath));

            @unlink($fileResultPath);
            return ['base64string' => $base64string, 'status' => 'success'];
        } else {
            return ['status' => 'error'];
        }
    }


    /**
     * Возвращает ссылку для скачивания
     * @param string $filename Имя файла без расширения
     * @return array ['urlDownload'=>$urlDownload, 'status'=>'success','size'=>$size] or ['status'=>'error']
     */
    public function getSrcDownload($filename)
    {

        $link = $this->link;

        $cd =  $_SERVER['DOCUMENT_ROOT'] . '/tmp';

        $fileResultPath = $cd . '/' . $filename . ".pdf";

        $command = "wkhtmltopdf " . $link . " " . $filename . ".pdf";

        $resExec = exec("cd " . $cd . '; ' . $command . '; ');

        if (file_exists($fileResultPath)) {

            $urlDownload = $_SERVER['HTTP_HOST'] . '/download/?filename=' . $filename;

            $size = filesize($fileResultPath);

            return ['urlDownload' => $urlDownload, 'status' => 'success', 'size' => $size];
        } else {

            return ['status' => 'error'];
        }
    }
}
