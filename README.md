Скрипт для конвертации html страниц в pdf файлы
===


## Для работы скрипта необходимо установить модуль wkhtmltopdf
Инструкция по установке 
<https://infoit.com.ua/linux/ustanovka-wkhtmltopdf-wkhtmltoimage-v-ubuntu-20-04-18-04-debian-10-9/>


## /lib/config.php
Здесь устанавливаем токен если нужно
```php
    <?

    define('TOKEN', 'ВАШ_ТОКЕН');
```

## Входные параметры запроса
Сервер принимает POST и GET запросы

Параметр  | Значение | Пример
----------------|----------------------|------------
`token`       | ваш токен | sdfasdfksdfnsd
`action`       | `getFile` (возвращает файл закодированыый в строку base64), `getSrcDownload` (возвращает ссылку для скачивания файла) | getFile
`link`   | Ссылка на страницу исходник  | https://google.com
`fileName`      | Желаемое имя файла  | myPdfFile



## Ответ в формате JSON







